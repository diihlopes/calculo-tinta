## Instruções de execução do projeto Laravel

1 - Ter o PHP 7.3+ e o Composer instalados.<br>
2 - Abra um terminal na pasta raiz do projeto.<br>
3 - Execute o comando composer install para instalar as dependências do projeto.<br>
4 - Execute o comando "php artisan serve" para iniciar o servidor local do Laravel.<br>
5 - Acesse o endereço http://localhost:8000/sala em seu navegador para ver a página inicial da calculadora.<br>

## Uso

Para utilizar a calculadora de tinta, basta preencher as medidas das paredes da sala na página inicial e clicar em "Calcular". A página de resultado mostrará a área total das paredes, a quantidade de tinta necessária em litros e a lista de latas de tinta necessárias.