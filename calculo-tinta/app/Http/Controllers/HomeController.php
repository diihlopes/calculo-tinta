<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('sala.index');
    }

    public function calcularTinta(Request $request)
    {
        // Validação dos dados do formulário
        $request->validate([
            'largura-parede-1' => 'required|numeric|min:1|max:50',
            'altura-parede-1' => 'required|numeric|min:1|max:50',
            'qtd-portas-parede-1' => 'required|numeric|min:0|max:10',
            'qtd-janelas-parede-1' => 'required|numeric|min:0|max:10',
            'largura-parede-2' => 'required|numeric|min:1|max:50',
            'altura-parede-2' => 'required|numeric|min:1|max:50',
            'qtd-portas-parede-2' => 'required|numeric|min:0|max:10',
            'qtd-janelas-parede-2' => 'required|numeric|min:0|max:10',
            'largura-parede-3' => 'required|numeric|min:1|max:50',
            'altura-parede-3' => 'required|numeric|min:1|max:50',
            'qtd-portas-parede-3' => 'required|numeric|min:0|max:10',
            'qtd-janelas-parede-3' => 'required|numeric|min:0|max:10',
            'largura-parede-4' => 'required|numeric|min:1|max:50',
            'altura-parede-4' => 'required|numeric|min:1|max:50',
            'qtd-portas-parede-4' => 'required|numeric|min:0|max:10',
            'qtd-janelas-parede-4' => 'required|numeric|min:0|max:10',
        ]);

        // Cálculo da área total das paredes
        $areaTotal = 0;
        for ($i = 1; $i <= 4; $i++) {
            $larguraParede = $request->input("largura-parede-$i");
            $alturaParede = $request->input("altura-parede-$i");
            $qtdPortas = $request->input("qtd-portas-parede-$i");
            $qtdJanelas = $request->input("qtd-janelas-parede-$i");

            // Cálculo da área da parede (descontando a área das portas e janelas)
            $areaParede = ($larguraParede * $alturaParede) - (($qtdPortas * 0.8 * 1.9) + ($qtdJanelas * 2 * 1.2));
            $areaParede = max(0, $areaParede); // Não permitir área negativa
            $areaTotal += $areaParede;
        }

        // Cálculo da quantidade de tinta necessária
        $qtdTinta = $areaTotal / 5;

        // Cálculo das latas de tinta necessárias
        $latas = [];
        while ($qtdTinta > 0) {
            if ($qtdTinta >= 18) {
                $latas[] = '18L';
                $qtdTinta -= 18;
            } elseif ($qtdTinta >= 3.6) {
                $latas[] = '3,6L';
                $qtdTinta -= 3.6;
            } elseif ($qtdTinta >= 2.5) {
                $latas[] = '2,5L';
                $qtdTinta -= 2.5;
            } else {
                $latas[] = '0,5L';
                $qtdTinta -= 0.5;
            }
        }

        // Retorna a view com as informações para o usuário
        return view('sala.resultado', [
            'areaTotal' => $areaTotal,
            'latas' => $latas,
        ]);
    }
}
