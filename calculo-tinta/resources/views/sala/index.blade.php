<!DOCTYPE html>
<html>
<head>
    <title>Calculadora de Tinta</title>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <h1>Calculadora de Tinta</h1>
    <form method="post" action="{{ route('sala.calcular') }}">
        @csrf

        <!-- Parede 1 -->
        <h2>Parede 1</h2>

        <label for="largura-parede-1">Largura:</label>
        <input type="number" name="largura-parede-1" id="largura-parede-1" required min="1" max="50"><br>

        <label for="altura-parede-1">Altura:</label>
        <input type="number" name="altura-parede-1" id="altura-parede-1" required min="1" max="50"><br>

        <label for="qtd-portas-parede-1">Quantidade de portas:</label>
        <input type="number" name="qtd-portas-parede-1" id="qtd-portas-parede-1" required min="0" max="10"><br>

        <label for="qtd-janelas-parede-1">Quantidade de janelas:</label>
        <input type="number" name="qtd-janelas-parede-1" id="qtd-janelas-parede-1" required min="0" max="10"><br>

        <!-- Parede 2 -->
        <h2>Parede 2</h2>

        <label for="largura-parede-2">Largura:</label>
        <input type="number" name="largura-parede-2" id="largura-parede-2" value="{{ old('largura-parede-2') }}" required>
        <br>

        <label for="altura-parede-2">Altura:</label>
        <input type="number" name="altura-parede-2" id="altura-parede-2" value="{{ old('altura-parede-2') }}" required>
        <br>

        <label for="qtd-portas-parede-2">Quantidade de portas:</label>
        <input type="number" name="qtd-portas-parede-2" id="qtd-portas-parede-2" value="{{ old('qtd-portas-parede-2') }}" required>
        <br>

        <label for="qtd-janelas-parede-2">Quantidade de janelas:</label>
        <input type="number" name="qtd-janelas-parede-2" id="qtd-janelas-parede-2" value="{{ old('qtd-janelas-parede-2') }}" required>
        <br>

        <!-- Parede 3 -->
        <h2>Parede 3</h2>
        <label for="largura-parede-3">Largura:</label>
        <input type="number" name="largura-parede-3" id="largura-parede-3" value="{{ old('largura-parede-3') }}" required>
        <br>

        <label for="altura-parede-3">Altura:</label>
        <input type="number" name="altura-parede-3" id="altura-parede-3" value="{{ old('altura-parede-3') }}" required>
        <br>

        <label for="qtd-portas-parede-3">Quantidade de portas:</label>
        <input type="number" name="qtd-portas-parede-3" id="qtd-portas-parede-3" value="{{ old('qtd-portas-parede-3') }}" required>
        <br>
        <label for="qtd-janelas-parede-3">Quantidade de janelas:</label>
        <input type="number" name="qtd-janelas-parede-3" id="qtd-janelas-parede-3" value="{{ old('qtd-janelas-parede-3') }}" required>
        <br>

        <!-- Parede 4 -->
        <h2>Parede 4</h2>
        <label for="largura-parede-4">Largura:</label>
        <input type="number" name="largura-parede-4" id="largura-parede-4" value="{{ old('largura-parede-4') }}" required>
        <br>

        <label for="altura-parede-4">Altura:</label>
        <input type="number" name="altura-parede-4" id="altura-parede-4" value="{{ old('altura-parede-4') }}" required>
        <br>

        <label for="qtd-portas-parede-4">Quantidade de portas:</label>
        <input type="number" name="qtd-portas-parede-4" id="qtd-portas-parede-4" value="{{ old('qtd-portas-parede-4') }}" required>
        <br>

        <label for="qtd-janelas-parede-4">Quantidade de janelas:</label>
        <input type="number" name="qtd-janelas-parede-4" id="qtd-janelas-parede-4" value="{{ old('qtd-janelas-parede-4') }}" required>
        <br><br>

        <input type="submit" value="Calcular">
    </form>
</body>
</html>