<!DOCTYPE html>
<html>
<head>
    <title>Resultado do cálculo de tinta</title>
</head>
<body>
    <h1>Resultado do cálculo de tinta</h1>
    <p>A área total das paredes da sala é: {{ $areaTotal }} metros quadrados</p>
    <p>A quantidade de tinta necessária é: {{ $areaTotal / 5 }} litros</p>
    <p>A lista de latas de tinta necessárias é:</p>
    <ul>
        @foreach ($latas as $lata)
            <li>{{ $lata }}</li>
        @endforeach
    </ul>
</body>
</html>